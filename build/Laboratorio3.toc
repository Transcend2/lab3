\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {spanish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\IeC {\'I}NDICE DE FIGURAS}{v}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 1. \hspace {0.2cm} \uppercase {Introducci\IeC {\'o}n}}{6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 2. \hspace {0.2cm} \uppercase {Objetivos}}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{2.1 \hspace {0.5cm}\uppercase {Objetivo general}}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{2.2 \hspace {0.5cm}\uppercase {Objetivos espec\IeC {\'\i }ficos}}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 3. \hspace {0.2cm} \uppercase {Marco te\IeC {\'o}rico}}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{3.1 \hspace {0.5cm}\uppercase {Compraci\IeC {\'o}n en la ejecuci\IeC {\'o}n}}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{3.2 \hspace {0.5cm}\uppercase {Ventajas de la programaci\IeC {\'o}n vectorial }}{10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 4. \hspace {0.2cm} \uppercase {Discusiones}}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.1 \hspace {0.5cm}\uppercase {Descripci\IeC {\'o}n de funciones}}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.2 \hspace {0.5cm}\uppercase {Primera funci\IeC {\'o}n}}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.2.1 \hspace {0.2cm} Forma escalar}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.2.2 \hspace {0.2cm} Forma vectorial}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.3 \hspace {0.5cm}\uppercase {Segunda funci\IeC {\'o}n}}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.3.1 \hspace {0.2cm} Forma escalar}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.3.2 \hspace {0.2cm} Forma vectorial}{14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.4 \hspace {0.5cm}\uppercase {Resultados}}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.4.1 \hspace {0.2cm} Funcion 1}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.4.2 \hspace {0.2cm} Funcion 2}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.5 \hspace {0.5cm}\uppercase {An\IeC {\'a}lisis comparativo}}{15}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 5. \hspace {0.2cm} \uppercase {Conclusi\IeC {\'o}n}}{16}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 6. \hspace {0.2cm} \uppercase {Referencias}}{17}
