#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <getopt.h>

float elevado(float base, int exp){ // una simple funcion que calcula una base elevado a un exponente
int i;
float res=1;
for(i=0;i<exp;i++){
    res = res*base;
}
return res;
}

void Funcion1(char* ruta){ // ruta es la direccion /nombre del archivo de texto par ala entrada
FILE * fichero;
char linea[10];
float sumatoria = 0;
	fichero = fopen(ruta, "r");
	if( fichero !=NULL){
		while(!feof(fichero)){
			memset(linea,'\0',sizeof(linea));
				fgets(linea,10,(FILE*)fichero);
				float i = atof(linea);
				sumatoria = sumatoria + elevado(sqrt(i),i);
        }
    }
    fclose(fichero);
    printf("La sumatoria de la funcion 1 es : %f",sumatoria);

}

void Funcion2(char*ruta){

FILE * fichero;
char linea[10];
float sumatoria =0;
int cantlineas =0;
    fichero = fopen(ruta,"r");
    if (fichero!= NULL){
        while(!feof(fichero)){
            memset(linea,'\0',sizeof(linea));
            fgets(linea,10,(FILE*)fichero);
            cantlineas++;
        }
    }
    fclose(fichero);
float arreglo[cantlineas];
    fichero = fopen(ruta,"r");
int i =0;
    if (fichero!=NULL){
        while(!feof(fichero)){
            memset(linea,'\0',sizeof(linea));
            fgets(linea,10,(FILE*)fichero);

            arreglo[i]= atof(linea);
            i++;
        }
    }

    for(i=0;i+1<cantlineas;i++){
        sumatoria =sumatoria +(arreglo[i]*arreglo[i+1]);

    }
    printf("La Sumatoria de la Funcion 2 es : %f",sumatoria);
}




int main(int argc, char **argv){
	char *f = NULL;
	int index;
	int c;
	opterr = 0; // Se le da el valor 0 para suprimir el mensaje de error generado por getopt cuando la funci�n no es reconocida.
	while ((c = getopt (argc, argv, "f:")) != -1) //Para reconocer entradas -i y -n con sus respectivos argumentos.
	switch (c){
		case 'f':
			f = optarg; // Recibe el argumento por consola.
			break;
		case '?':
			if (optopt == 'f'){ // Para opcion no reconocida o erroneamente escrita
				fprintf (stderr,"La opci�n -%c requiere un argumento.\n", optopt);
			}else{
				fprintf (stderr, "Opci�n `-%c' desconocida, debe usar las opcion '-f' para se�alar el archivo de texto el cual leer.\n", optopt);
			}
		default:
			printf("ERROR");
	}
    f="oli.txt";
	if(f!=NULL ){
       // printf("5 elevado a 2 %f",elevado(5,2));

        Funcion1(f);
        printf("\n");
        Funcion2(f);
		}
	for (index = optind; index < argc; index++)// Opciones escritas si el formato requerido
	printf ("Opci�n '%s' no v�lida, debe utilizar el formato '-<operaci�n>' para agregar sus par�metros\n", argv[index]);
return 0;
}
