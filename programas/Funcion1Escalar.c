#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <getopt.h>

float elevado(float base, int exp){ // una simple funcion que calcula una base elevado a un exponente
int i;
float res=1;
for(i=0;i<exp;i++){
    res = res*base;
}
return res;
}

void Funcion1(char* ruta){ // ruta es la direccion /nombre del archivo de texto par ala entrada
FILE * fichero;
char linea[10];
float sumatoria = 0;
	fichero = fopen(ruta, "r");
	if( fichero !=NULL){
		while(!feof(fichero)){
			memset(linea,'\0',sizeof(linea));
				fgets(linea,10,(FILE*)fichero);
				float i = atof(linea);
				sumatoria = sumatoria + elevado(sqrt(i),i);
        }
    }
    fclose(fichero);
    printf("La sumatoria de la funcion 1 es : %f",sumatoria);

}

int main(int argc, char **argv){
	char *f = NULL;
	int index;
	int c;
	opterr = 0; // Se le da el valor 0 para suprimir el mensaje de error generado por getopt cuando la función no es reconocida.
	while ((c = getopt (argc, argv, "f:")) != -1) //Para reconocer entradas -i y -n con sus respectivos argumentos.
	switch (c){
		case 'f':
			f = optarg; // Recibe el argumento por consola.
			break;
		case '?':
			if (optopt == 'f'){ // Para opcion no reconocida o erroneamente escrita
				fprintf (stderr,"La opción -%c requiere un argumento.\n", optopt);
			}else{
				fprintf (stderr, "Opción `-%c' desconocida, debe usar las opcion '-f' para señalar el archivo de texto el cual leer.\n", optopt);
			}
		default:
			printf("ERROR");
	}
	if(f!=NULL ){
        Funcion1(f);
		}
	for (index = optind; index < argc; index++)// Opciones escritas si el formato requerido
	printf ("Opción '%s' no válida, debe utilizar el formato '-<operación>' para agregar sus parámetros\n", argv[index]);
return 0;
}
