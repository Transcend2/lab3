#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>

int comparar (const void * a, const void * b){ // Para comparar dos numeros, funcion utlizada en el quicksort (qsort)
   return ( *(float*)a - *(float*)b );
}

void Funcion1_Vectorial(char *ruta){ // La entrada es la ruta o nombre del archivo en el caso que este en el directorio raiz
FILE * fichero;
char linea[10];
int cantlineas =0;
    fichero = fopen(ruta,"r");
    if (fichero!= NULL){
        while(!feof(fichero)){  // en esta parte se cuentan las lineas del archivo para saber el tamaño del arreglo
            memset(linea,'\0',sizeof(linea));
            fgets(linea,10,(FILE*)fichero);
            cantlineas++; // Variable que cuenta la lineas 
        }
    }
    fclose(fichero);
    int i = 0;
    float Preload[cantlineas+3] __attribute__((aligned(16))); // el arreglo que contendra los numeros leidos desde el archivo , es de tamaño cantlineas+3 para evitar multiplicaciones por basura
    fichero = fopen(ruta,"r");
    if(fichero!=NULL) {
        while(!feof(fichero)){
            memset(linea,'\0',sizeof(linea));
            fgets(linea,10,(FILE*)fichero);
            Preload[i] = atof(linea); // se pasa lo leido de formato string a float en el arreglo posicion "i" 
                       i++;
        }
        // Para evitar cargar basura en el vector si la lista de numeros no es multiplo de 4
        Preload[cantlineas]=0;
        Preload[cantlineas+1]=0;
        Preload[cantlineas+2]=0;
        Preload[cantlineas+3]=0;
    }
    fclose(fichero);
    __m128 resultado = _mm_setzero_ps();
    for(i=0;i<cantlineas;i=i+4){
        __m128 v1 =_mm_load_ps(&Preload[i]); // vector 1 se cargan 4 valores del arreglo Preload
        float Aux[4] __attribute__((aligned(16))); // auxiliar para poder ordenar los numeros
        _mm_store_ps(Aux,v1);
        qsort(Aux,4,sizeof(float),comparar); // funcion Quicksort que 
        v1 = _mm_load_ps(&Aux[0]);
        __m128 acc =_mm_load_ps(&Aux[0]);
        int uno = (float)Aux[0];
        int dos = (float)Aux[1];
        int tres = (float)Aux[2];
        int cuatro = (float)Aux[3];
        int j;
            for(j=0;j<uno;j++){
                acc = _mm_mul_ps(acc,v1);
            }
            if(Aux[0]!=0){
                Aux[0]=1;
            }
        v1 = _mm_load_ps(&Aux[0]);
            if(uno>0){
                for(j=0;j<(dos-uno);j++){
                    acc = _mm_mul_ps(acc,v1);
                }
            }
            if(Aux[1]!=0){
                Aux[1]=1;
            }
        v1 = _mm_load_ps(&Aux[0]);
            if(dos>0){
                for(j=0;j<(tres-dos);j++){
                    acc = _mm_mul_ps(acc,v1);
                }
            }
            if(Aux[2]!=0){
                Aux[2]=1;
            }
        v1 = _mm_load_ps(&Aux[0]);
            if(tres>0){
                for(j=0;j<(cuatro-tres);j++){
                    acc = _mm_mul_ps(acc,v1);
                }
            }
        acc = _mm_sqrt_ps(acc);
        resultado = _mm_add_ps(resultado,acc);
    }
    _mm_store_ps(Preload, resultado);

    float resFinal = Preload[0] + Preload[1] + Preload [2] + Preload[3];
    printf("El Resultado de la Funcion 1 de forma vectorial es : %f ",resFinal);
}

void Funcion2_Vectorial(char *ruta){
FILE * fichero;
char linea[10];
int cantlineas =0;
    fichero = fopen(ruta,"r");
    if (fichero!= NULL){
        while(!feof(fichero)){
            memset(linea,'\0',sizeof(linea));
            fgets(linea,10,(FILE*)fichero);
            cantlineas++;
        }
    }
    fclose(fichero);
    int i = 0;
    float Preload[cantlineas+3] __attribute__((aligned(16)));
    float Preload2[cantlineas+3] __attribute__((aligned(16)));
    fichero = fopen(ruta,"r");
    if(fichero!=NULL) {
        while(!feof(fichero) && i<cantlineas){
            memset(linea,'\0',sizeof(linea));
            fgets(linea,10,(FILE*)fichero);
            Preload[i] = atof(linea);
            if(i!=0){

               Preload2[i-1] = atof(linea);
            }
            i++;
        }
        // la parte aconitnuacion es para evitar que el vector carge basura cuando la lista no es un multimplo de 4b
        Preload[i]=0;
        Preload[i+1]=0;
        Preload[i+2]=0;
        Preload[i+3]=0;
        Preload2[i-1]=0;
        Preload2[i]=0;
        Preload2[i+1]=0;
        Preload2[i+2]=0;

    }
    fclose(fichero);
    __m128 Sumatoria = _mm_setzero_ps();
    for(i=0;i<cantlineas;i= i+4){
       __m128 v1 =_mm_load_ps(&Preload[i]); // carga del vector desde el arreglo Preload, que contiene los numeros leidos del archivo en orden
       __m128 v2 =_mm_load_ps(&Preload2[i]);// carga del vector desde el arreglo Preload2, que contiene los numeros corrido 1 espacio ,saltandose el primer numero de la lista

        Sumatoria = _mm_add_ps(Sumatoria,_mm_mul_ps(v1,v2));

    }
    _mm_store_ps(Preload, Sumatoria);

    float resFinal = Preload[0] + Preload[1] + Preload [2] + Preload[3];
    printf("El Resultado de la Funcion 2 de forma vectorial es : %f ",resFinal);
}

int main(int argc, char **argv){
	char *f = NULL;
	int index;
	int c;
	opterr = 0; // Se le da el valor 0 para suprimir el mensaje de error generado por getopt cuando la función no es reconocida.
	while ((c = getopt (argc, argv, "f:")) != -1) //Para reconocer entradas -i y -n con sus respectivos argumentos.
	switch (c){
		case 'f':
			f = optarg; // Recibe el argumento por consola.
			break;
		case '?':
			if (optopt == 'f'){ // Para opcion no reconocida o erroneamente escrita
				fprintf (stderr,"La opción -%c requiere un argumento.\n", optopt);
			}else{
				fprintf (stderr, "Opción `-%c' desconocida, debe usar las opcion '-f' para señalar el archivo de texto el cual leer.\n", optopt);
			}
		default:
			printf("ERROR");
	}
	if(f!=NULL ){
       // printf("5 elevado a 2 %f",elevado(5,2));

		Funcion1_Vectorial(f);

		}
	for (index = optind; index < argc; index++)// Opciones escritas si el formato requerido
	printf ("Opción '%s' no válida, debe utilizar el formato '-<operación>' para agregar sus parámetros\n", argv[index]);
return 0;
}
